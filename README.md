# Workflow

1. Make sure `main` is up to date
2. Create a feature branch for the update
3. Make your changes in `source/`
4. **Commit** your changes.
5. Run the deploy-to-staging script.
6. Verify the page.
7. Assuming it looks good, merge your feature branch to main.
8. Once merged, run the deploy-to-production script.

## To un-deploy:

* Use `undeploy-production` to revert the production repo _and_ the staging branch to the previous production site.  This can only undo the most recent deployment!
* Use `undeploy-staging` to revert the staging branch to the previous production site _and_ the source branch to the previous staging deployment.  This can only undo the most recent deployment!

# Git conventions

## Branches

* `main` holds the deployment scripts and the site source.
* `pages` is the staging/testing area for the html site.
* `preprod` is the production pre-deployment area for the html site.

## Tags

* The `DEPLOYMENT` tag points to the last source commit that was deployed to staging and production-pages; `DEPLOYMENT-prev` points to the previous DEPLOYMENT tag.
* The `STAGING` tag points to the last commit staged to the staging/testing area, and `STAGING-prev` to the previous STAGING tag.
* The `PREPRODUCTION` tag points to the last commit staged to the production pre-deployment area, and `PREPRODUCTION-prev` to the previous PREPRODUCTION tag.
* The `PRODUCTION` tag points to the last production pre-deployment commit that was pushed to the production repo, and `PRODUCTION-prev` to the previous PRODUCTION tag.

# Initial Setup

* Create `pages` and `preprod` branches off of the initial repo commit.
* Create `.domains` file in `preprod` branch
* Create the `DEPLOYMENT` tag pointing to `main`.
* Create the `STAGING` tag pointing to `pages`.
* Create the `PREPRODUCTION` and `PRODUCTION` tags pointing to `preprod`.
* Create the `production` remote origin pointing to the production git repo.
* Force-push the `preprod` branch to the `production` origin's `main` branch.

```
git branch pages initial
git push --force -u origin pages
git checkout -b preprod initial
cat > .domains << __EOF__
designedforplay.games
www.designedforplay.games
designedforplaygames.codeberg.page
__EOF__
git add .domains
git commit -m 'Initial `.domains` file'
git push --force -u origin preprod
git checkout main
git tag DEPLOYMENT main
git tag STAGING pages
git tag PREPRODUCTION preprod
git tag PRODUCTION preprod
git push --force origin tag DEPLOYMENT tag STAGING tag PREPRODUCTION tag PRODUCTION
git remote add production "$(git remote get-url origin | sed -e 's;/[^/]*[.]git$;/pages.git;')"
git push --force production preprod:main
```
