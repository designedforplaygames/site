#!/bin/bash -ex

# This is the same as `deploy-to-production.sh` but does not update the `-prev` tag.

# switch to our top level directory
cd "$(git rev-parse --show-toplevel)"

# load in the common deployment parameters
. "params.sh"

# update the production origin
git fetch --prune ${PROD_ORIGIN}

# push the production branch to the production origin
# do NOT use force, so a non-fast-forward will fail
# do NOT use `-u`, since we *do not* want to track the production origin
git push ${PROD_ORIGIN} ${PREPROD_BRANCH}:${PROD_BRANCH}

# update production tags
git tag -f ${PROD_TAG} ${PREPROD_BRANCH}

# push production tags
git push --force ${SOURCE_ORIGIN} tag ${PROD_TAG}
