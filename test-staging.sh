#!/bin/bash
set -ex

# verify no modified files in the repo

# switch to our top level directory
cd "$(git rev-parse --show-toplevel)"

# load in the common deployment parameters
. "params.sh"

# determine the repo names for our source and production origins.

STAGING_REPO_NAME="$( \
    git remote get-url ${STAGING_ORIGIN} \
    | sed -e 's;^.*/;;' -e 's;[.]git$;;' \
)"
PROD_REPO_NAME="$( \
    git remote get-url ${PROD_ORIGIN} \
    | sed -e 's;^.*/;;' -e 's;[.]git$;;' \
)"

# determine the path prefix for source and production
# the // is needed because of git-bash's unix-to-windows path conversion
if [[ "${STAGING_REPO_NAME}" != "pages" ]]; then
    STAGING_PATH_PREFIX="${STAGING_REPO_NAME}"
fi
if [[ "${PROD_REPO_NAME}" != "pages" ]]; then
    PROD_PATH_PREFIX="${PROD_REPO_NAME}"
fi

# figure out how to run docker.  rancher desktop creates both `docker`
# and `nerdctl`, but `docker` doesn't seem to work properly in a bash
# script, so prefer `nerdctl` if it exists.

docker=docker
[[ -z "$(which nerdctl)" ]] || docker="nerdctl -n buildkit"

# Generate the log message that will be used for both the staging and 
# preprod deployment.
DEPLOYMENT_LOG_MESSAGE="$( \
    git log \
        --reverse \
        --pretty=format:%s \
        ${SOURCE_TAG}..head \
        -- ${SOURCE_DIR} \
)"

# generate_site <target-branch> <target-tag> <target-origin> <target-path-prefix>
# * Empty and re-create the `.site` directory which will hold
#   the generated site files.
# * Run eleventy to generate the site files into `.site`
#   with the path prefix, if given.
# * Check out the target branch, empty out all the (non-dot) files,
#   and copy the site into it.
# * Commit the site contents using the previously-generated log message.
# * Update the tags and push tags and content.
# * Return to the original branch.

generate_site() {

    # Empty out and re-create the `.site` directory which will hold the
    # generated site files.

    rm -fr .site
    mkdir -p .site

    # Run eleventy to generate the site
    ${docker} run \
        --mount "type=bind,source=$(pwd)/${SOURCE_DIR},destination=/source,readonly" \
        --mount "type=bind,source=$(pwd)/.site,destination=/site" \
        --env "TARGET_BRANCH=$1" \
        --env "TARGET_TAG=$2" \
        --env "TARGET_ORIGIN=$3" \
        --env "TARGET_PATHPREFIX=$4" \
        --env "DEBUG=Eleventy*" \
    ${DOCKER_ELEVENTY:-darkfoxprime-eleventy} ${4:+--pathprefix=//$4}
}

# Test as if deploying to the staging area
generate_site ${STAGING_BRANCH} ${STAGING_TAG} ${STAGING_ORIGIN} ${STAGING_PATH_PREFIX}
