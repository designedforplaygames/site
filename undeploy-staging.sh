#!/bin/bash -ex

# verify no modified files in the repo

if [[ -n "$(git status --porcelain --untracked=no)" ]]; then
    echo "Modified files found in git repo!  Commit or stash!" 1>&2
    exit 1
fi

# switch to our top level directory
cd "$(git rev-parse --show-toplevel)"

# load in the common deployment parameters
. "params.sh"

# Ensure that no matter what happens, we end up in the current branch.
trap "git checkout \"$(git branch --show-current)\"" 0

# undeploy_site <target-branch> <target-tag> <target-origin>
# * Check out the target branch and reset it back to the previous
#   <target-tag>.
# * Update <target-tag>.
# * Push tags and content.
# * Return to the original branch.

undeploy_site() {

    # check out the target branch, empty out all the (non-dot) files,
    # and copy the site into it.

    git checkout "$1"

    # Reset back to previous tag.
    git reset --hard ${2}-prev

    # Update the tags and push.
    git tag -f ${2} ${2}-prev
    git push --force ${3} "${1}" tag "${2}" tag "${2}-prev"

    # return the the previous branch
    git checkout -
}

# Undeploy the staging area
undeploy_site ${STAGING_BRANCH} ${STAGING_TAG} ${STAGING_ORIGIN}
# Undeploy the pre-prod area
undeploy_site ${PREPROD_BRANCH} ${PREPROD_TAG} ${PREPROD_ORIGIN}

# Roll back the source tag to the previous tag
git tag -f ${SOURCE_TAG} ${SOURCE_TAG}-prev

# push the previous source tag
git push --force ${SOURCE_ORIGIN} tag ${SOURCE_TAG}
