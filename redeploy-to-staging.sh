#!/bin/bash -ex

# this is the same as `deploy-to-staging.sh` except it doesn't update the `-prev` tags

# verify no modified files in the repo

if [[ -n "$(git status --porcelain)" ]]; then
    echo "Modified or untracked files found in git repo!  Commit or stash!" 1>&2
    exit 1
fi

# switch to our top level directory
cd "$(git rev-parse --show-toplevel)"

# load in the common deployment parameters
. "params.sh"

# determine the repo names for our source and production origins.

STAGING_REPO_NAME="$( \
    git remote get-url ${STAGING_ORIGIN} \
    | sed -e 's;^.*/;;' -e 's;[.]git$;;' \
)"
PROD_REPO_NAME="$( \
    git remote get-url ${PROD_ORIGIN} \
    | sed -e 's;^.*/;;' -e 's;[.]git$;;' \
)"

# determine the path prefix for source and production
# the // is needed because of git-bash's unix-to-windows path conversion
if [[ "${STAGING_REPO_NAME}" != "pages" ]]; then
    STAGING_PATH_PREFIX="${STAGING_REPO_NAME}"
fi
if [[ "${PROD_REPO_NAME}" != "pages" ]]; then
    PROD_PATH_PREFIX="${PROD_REPO_NAME}"
fi

# figure out how to run docker.  rancher desktop creates both `docker`
# and `nerdctl`, but `docker` doesn't seem to work properly in a bash
# script, so prefer `nerdctl` if it exists.

docker=docker
[[ -z "$(which nerdctl)" ]] || docker="nerdctl -n buildkit"

# Ensure that no matter what happens, we end up in the current branch.
trap "git checkout \"$(git branch --show-current)\"" 0

# Generate the log message that will be used for both the staging and 
# preprod deployment.
DEPLOYMENT_LOG_MESSAGE="$( \
    git log \
        --reverse \
        --pretty=format:%s \
        ${SOURCE_TAG}..head \
        -- ${SOURCE_DIR} \
)"

# generate_site <target-branch> <target-tag> <target-origin> <target-path-prefix>
# * Empty and re-create the `.site` directory which will hold
#   the generated site files.
# * Run eleventy to generate the site files into `.site`
#   with the path prefix, if given.
# * Check out the target branch, empty out all the (non-dot) files,
#   and copy the site into it.
# * Commit the site contents using the previously-generated log message.
# * Update the tags and push tags and content.
# * Return to the original branch.

generate_site() {

    # Empty out and re-create the `.site` directory which will hold the
    # generated site files.

    rm -fr .site
    mkdir -p .site

    # Run eleventy to generate the site
    ${docker} run \
        --mount "type=bind,source=$(pwd)/${SOURCE_DIR},destination=/source,readonly" \
        --mount "type=bind,source=$(pwd)/.site,destination=/site" \
        --env "TARGET_BRANCH=$1" \
        --env "TARGET_TAG=$2" \
        --env "TARGET_ORIGIN=$3" \
        --env "TARGET_PATHPREFIX=$4" \
    ${DOCKER_ELEVENTY:-darkfoxprime-eleventy} ${4:+--pathprefix=//$4}

    # check out the target branch, empty out all the (non-dot) files,
    # and copy the site into it.

    git checkout "$1"

    [[ -z "$(ls)" ]] || git rm -rf *
    tar cf - -C .site . | tar xpf -
    rm -fr .site

    # Add the site and commit it using the previously-generated
    # log message.

    git add .
    git commit -m "${DEPLOYMENT_LOG_MESSAGE}"

    # Update the tags and push the branch and tags
    git tag -f ${2}
    git push --force ${3} "${1}" tag "${2}"

    # return the the previous branch
    git checkout -
}

# Deploy into the staging area
generate_site ${STAGING_BRANCH} ${STAGING_TAG} ${STAGING_ORIGIN} ${STAGING_PATH_PREFIX}
# Deploy into the pre-prod area using the *production* path prefix
generate_site ${PREPROD_BRANCH} ${PREPROD_TAG} ${PREPROD_ORIGIN} ${PROD_PATH_PREFIX}

# Update the source tags
git tag -f ${SOURCE_TAG}

# push the source tags
git push --force ${SOURCE_ORIGIN} tag ${SOURCE_TAG}
