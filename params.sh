#!/bin/false

# Source settings:
#   SOURCE_DIR = the directory, relative to the top level, which contains the site source.
#   SOURCE_ORIGIN = the remote origin which hosts the source branch.
#   SOURCE_TAG = the tag which tracks the source commit last deployed to the staging site.
SOURCE_DIR=source
SOURCE_ORIGIN=origin
SOURCE_TAG=DEPLOYMENT

# Staging settings:
#   STAGING_BRANCH = the branch name which holds the staging site.
#   STAGING_ORIGIN is the same as SOURCE_ORIGIN
#   STAGING_TAG = the tag which tracks the last deployment to the staging site.
STAGING_BRANCH=pages
STAGING_ORIGIN=${SOURCE_ORIGIN}
STAGING_TAG=STAGING

# PreProduction settings:
#   PREPROD_BRANCH = the branch name which holds the pre-production staging area.
#   PREPROD_ORIGIN is the same as SOURCE_ORIGIN
#   PREPROD_TAG = the tag which tracks the last deployment to the pre-production staging area.
PREPROD_BRANCH=preprod
PREPROD_ORIGIN=${SOURCE_ORIGIN}
PREPROD_TAG=PREPRODUCTION

# Production settings:
#   PROD_BRANCH = the branch name (in PROD_ORIGIN) which holds the production site.
#   PROD_ORIGIN = the remote origin which hosts PROD_BRANCH.
#   PROD_TAG = the tag which tracks (in PREPROD_BRANCH) the last deployment to production.
PROD_BRANCH=main
PROD_ORIGIN=production
PROD_TAG=PRODUCTION

# Docker image for Eleventy
DOCKER_ELEVENTY=designedforplaygames-eleventy
