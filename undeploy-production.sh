#!/bin/bash -ex

# switch to our top level directory
cd "$(git rev-parse --show-toplevel)"

# load in the common deployment parameters
. "params.sh"

# update the production origin
git fetch --prune ${PROD_ORIGIN}

# revert the production branch back to the previous production tag
# use force, since're reverting
# do NOT use `-u`, since we *do not* want to track the production origin
git push --force ${PROD_ORIGIN} ${PROD_TAG}-prev:${PROD_BRANCH}

# update production tags
git tag -f ${PROD_TAG} ${PROD_TAG}-prev

# push production tags
git push --force ${SOURCE_ORIGIN} tag ${PROD_TAG}
