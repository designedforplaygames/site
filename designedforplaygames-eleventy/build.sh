#!/bin/bash -x

docker=docker
[[ -z "$(which nerdctl)" ]] || docker="nerdctl -n buildkit"

${docker} build -t designedforplaygames-eleventy .
